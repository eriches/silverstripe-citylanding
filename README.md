# SilverStripe City landing #

City landing pages SilverStripe.

### Requirements ###

SilverStripe 4

### Version ###

Using Semantic Versioning.

### Installation ###

Install via Composer:

composer require "hestec/silverstripe-citylanding": "1.*"

### Configuration ###

do a dev/build and flush.

### Usage ###



### Issues ###

No known issues.

### Todo ###
