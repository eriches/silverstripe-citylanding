<?php

namespace Hestec\CityLanding;

use SilverStripe\ORM\DataObject;

class Province extends DataObject {

    private static $table_name = 'CityLandingProvince';

    private static $singular_name = 'Province';
    private static $plural_name = 'Provinces';

    private static $db = array(
        'Name' => 'Varchar(255)',
    );

    private static $has_many = array(
        'Cities' => City::class
    );

    private static $summary_fields = [
        'Name' => 'Name'
    ];

}