<?php

namespace Hestec\CityLanding;

use SilverStripe\ORM\DataObject;

class City extends DataObject {

    private static $table_name = 'CityLandingCity';

    private static $singular_name = 'City';
    private static $plural_name = 'Cities';

    private static $db = array(
        'Type' => "Enum('CITY,MUNICIPALITY','')",
        'Name' => 'Varchar(255)',
        'UrlId' => 'Varchar(255)',
        'Zip' => 'MultiValueField',
        'Wikipedia' => 'Varchar(255)',
        'CityInfo' => 'HTMLText',
        'MunicipalityWebsite' => 'Varchar(255)',
        'MunicipalityID' => 'Int',
        'Enabled' => 'Boolean'
    );

    private static $defaults = array(
        'Enabled' => true
    );

    private static $has_one = array(
        'Province' => Province::class
    );

    private static $summary_fields = [
        'Name' => 'Name'
    ];

    public function UrlExistsCheck($url) {
        $array = get_headers($url);
        $string = $array[0];
        if(strpos($string,"200") || strpos($string,"301")) {
            return true;
        } else {
            return false;
        }
    }

    public function MunicipalityWebsiteLink() {

        if (filter_var($this->MunicipalityWebsite, FILTER_VALIDATE_URL) && $this->UrlExistsCheck($this->MunicipalityWebsite)){

            return $this->MunicipalityWebsite;

        }

        return false;

    }

    public function WikipediaLink() {

        if (filter_var($this->Wikipedia, FILTER_VALIDATE_URL) && $this->UrlExistsCheck($this->Wikipedia)){

            return $this->Wikipedia;

        }elseif ($this->UrlExistsCheck('https://nl.wikipedia.org/wiki/'.str_replace(' ', '_', $this->Name))){

            return 'https://nl.wikipedia.org/wiki/'.str_replace(' ', '_', $this->Name);

        }

        return false;


    }

    public function GoogleMapName() {

        return str_replace("'", '', $this->Name);

    }

}