<?php

//namespace Hestec\KiesZekerAffiliate;

use SilverStripe\CMS\Model\SiteTree;
use Hestec\KiesZekerAffiliate\ComparisonAllInOnePage;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;

class ImportController extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'Import',
        'ImportGemeente',
        'test'
    );

    public function Import() {

        $cities = DB::query("SELECT * FROM CITIES WHERE PROVINCIE = 'Flevoland'");

        foreach ($cities as $node){

            $import = new \Hestec\CityLanding\City();
            $import->Name = $node['PLAATS'];
            $import->UrlId = SiteTree::create()->generateURLSegment($node['PLAATS']);
            $import->Type = "CITY";
            $import->ProvinceID = 12;
            $import->MunicipalityID = $node['GEMEENTE_ID'];
            $postcode = array();
            if (strlen($node['POSTCODE']) == 4){
                array_push($postcode, $node['POSTCODE']);
            }
            if (strlen($node['POSTCODE']) == 9){

                $first = substr($node['POSTCODE'], 0, 4);
                $last = substr($node['POSTCODE'], 5);

                while ($first <= $last){

                    array_push($postcode, $first);
                    $first++;

                }

            }
            $import->Zip = $postcode;


            $import->write();

        }


        return "klaar";

    }

    public function ImportGemeente() {

        $cities = DB::query("SELECT * FROM CITIES WHERE PROVINCIE = 'Flevoland' GROUP BY GEMEENTE");

        foreach ($cities as $node){

            $import = new \Hestec\CityLanding\City();
            $import->Name = "Gemeente ".$node['GEMEENTE'];
            $import->UrlId = SiteTree::create()->generateURLSegment("Gemeente ".$node['GEMEENTE']);
            $import->MunicipalityWebsite = $node['GEMEENTE_URL'];
            $import->Type = "MUNICIPALITY";
            $import->ProvinceID = 12;


            $import->write();

            DB::query("UPDATE CITIES SET GEMEENTE_ID = ".$import->ID."  WHERE GEMEENTE = '".$node['GEMEENTE']."'");

        }


        return "klaar";

    }

}
