<?php

namespace Hestec\CityLanding;

use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ProvincePage extends \Page {

    private static $table_name = 'CityLandingProvincePage';

    private static $db = array(
        'CitiesIntro' => 'HTMLText'
    );

    private static $defaults = array(
    );

    private static $has_one = array(
        'Province' => Province::class
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ProvinceSource = Province::get()->map('ID', 'Name');

        $ProvinceField = DropdownField::create('ProvinceID', "Province", $ProvinceSource);
        $CitiesIntroField = HTMLEditorField::create('CitiesIntro', "CitiesIntro");

        $fields->addFieldsToTab(
            'Root.Main', [
                $ProvinceField
            ]
        );

        $fields->addFieldsToTab(
            'Root.ExtraContent', [
                $CitiesIntroField
            ]
        );


        return $fields;
    }

}