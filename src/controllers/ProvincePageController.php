<?php

namespace Hestec\CityLanding;

class ProvincePageController extends \PageController {

    public function getMunicipalities(){

        return $this->Province()->Cities()->filter(array('Type' => 'MUNICIPALITY'));

    }

    public function getMunicipalityCities($MunicipalityID){

        return City::get()->filter(array('MunicipalityID' => $MunicipalityID));

    }

}